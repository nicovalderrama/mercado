package mercado.PaqueteLecturas;

import java.time.LocalDate;

public class Lectura {

    private int consumoActual;
    private LocalDate fechaLectura;
    public Medidor medidor;

    public Lectura(int consumoActual, LocalDate fechaLectura, Medidor medidor) {
        this.consumoActual = consumoActual;
        this.medidor = medidor;
        this.fechaLectura = fechaLectura;
    }

    public int getConsumoActual() {
        return consumoActual;
    }

    public void setConsumoActual(int consumoActual) {
        this.consumoActual = consumoActual;
    }

    public Medidor getMedidor() {
        return medidor;
    }

    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }

    public LocalDate getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(LocalDate fechaLectura) {
        this.fechaLectura = fechaLectura;
    }

    public boolean equals(Object l) {
        Lectura comparacion = (Lectura) l;
        return this.getFechaLectura().equals(comparacion.getFechaLectura());
    }

}
