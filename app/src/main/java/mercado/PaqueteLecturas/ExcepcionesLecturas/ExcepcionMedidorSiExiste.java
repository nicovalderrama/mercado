package mercado.PaqueteLecturas.ExcepcionesLecturas;


public class ExcepcionMedidorSiExiste extends RuntimeException {
    
    public ExcepcionMedidorSiExiste()
    {
        super("El medidor ya existe");
    }
}
