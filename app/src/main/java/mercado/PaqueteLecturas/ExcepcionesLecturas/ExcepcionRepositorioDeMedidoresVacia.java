package mercado.PaqueteLecturas.ExcepcionesLecturas;

public class ExcepcionRepositorioDeMedidoresVacia extends Exception {
    
    public ExcepcionRepositorioDeMedidoresVacia ()
    {
        super("El repositorio no tiene ningun medidor registrado");
    }
}
