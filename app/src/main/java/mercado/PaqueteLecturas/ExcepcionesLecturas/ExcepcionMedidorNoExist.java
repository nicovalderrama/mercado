package mercado.PaqueteLecturas.ExcepcionesLecturas;

public class ExcepcionMedidorNoExist extends RuntimeException {

    public ExcepcionMedidorNoExist()
    {
        super("El medidor no existe");
    }
    
}
