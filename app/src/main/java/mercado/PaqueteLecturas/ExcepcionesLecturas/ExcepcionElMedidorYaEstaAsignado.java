package mercado.PaqueteLecturas.ExcepcionesLecturas;

public class ExcepcionElMedidorYaEstaAsignado extends Exception {
    
    public ExcepcionElMedidorYaEstaAsignado()
    {
        super("Este medidor ya esta asignado a un puesto");
    }
}
