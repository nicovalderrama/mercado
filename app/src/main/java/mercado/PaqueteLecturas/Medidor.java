package mercado.PaqueteLecturas;

import java.util.ArrayList;

import mercado.PaqueteLecturas.ExcepcionesLecturas.ExcepcionElMedidorYaEstaAsignado;
import mercado.PaqueteSectores.Puesto;

public class Medidor {
    private Integer numeroDeMedidor;
    private ArrayList<Lectura> lecturas = new ArrayList<Lectura>();
    private Puesto puesto;

    public Medidor(int numeroDeMedidor, ArrayList<Lectura> lecturas) {
        this.numeroDeMedidor = numeroDeMedidor;
        this.lecturas = lecturas;
    }

    public Integer getNumeroDeMedidor() {
        return numeroDeMedidor;
    }

    public void setNumeroDeMedidor(Integer numeroDeMedidor) {
        this.numeroDeMedidor = numeroDeMedidor;
    }

    public ArrayList<Lectura> getLecturas() {
        return lecturas;
    }

    public void setLecturas(ArrayList<Lectura> lecturas) {
        this.lecturas = lecturas;
    }

    public void addLectura(Lectura lectura) {
        lecturas.add(lectura);
    }

    
    public boolean equals(Object m) {
        Medidor comparacion = (Medidor) m;
        return this.getNumeroDeMedidor().equals(comparacion.getNumeroDeMedidor());
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto)throws ExcepcionElMedidorYaEstaAsignado {
        if(this.tieneUnPuestoAsignado())
            throw new ExcepcionElMedidorYaEstaAsignado();
        else    
            this.puesto = puesto;
    }

    public boolean tieneUnPuestoAsignado()
    {
        return this.puesto != null;
    }


    
}
