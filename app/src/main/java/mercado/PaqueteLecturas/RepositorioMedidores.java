package mercado.PaqueteLecturas;

import java.util.ArrayList;

import mercado.PaqueteLecturas.ExcepcionesLecturas.*;

public class RepositorioMedidores {

    private ArrayList<Medidor> medidores;

    public RepositorioMedidores() {
        this.medidores = new ArrayList<>();
    }

    public ArrayList<Medidor> getMedidores() throws ExcepcionRepositorioDeMedidoresVacia {
        if(medidores.isEmpty())
            throw new ExcepcionRepositorioDeMedidoresVacia();
        else    
            return medidores;
    }

    public void setMedidores(ArrayList<Medidor> medidores) {
        this.medidores = medidores;
    }

    public void agregarMedidor(Medidor medidor) throws ExcepcionMedidorSiExiste {

            if(medidores.isEmpty())
                medidores.add(medidor);
            else
                {
                    if(medidores.contains(medidor))
                        throw new ExcepcionMedidorSiExiste();
                    else
                        medidores.add(medidor);

                }
    }

    public void eliminarMedidor(Integer numeroDeMedidor) throws ExcepcionMedidorNoExist {
        for (int i=0; i<medidores.size();i++ ) {
            Medidor medidorABorrar = medidores.get(i);
            if (medidorABorrar.getNumeroDeMedidor() == numeroDeMedidor) {
                medidores.remove(i);
            } else {
                if(i>=medidores.size())
                    throw new ExcepcionMedidorNoExist();
            }
        }
    }

    public void modificarMedidor(Integer numeroDeMedidor, Integer nuevoNumeroDeMedidor) throws ExcepcionMedidorNoExist
    {
        for (int i=0; i<medidores.size();i++ ) {
            Medidor medidorAModificar = medidores.get(i);
            if (medidorAModificar.getNumeroDeMedidor() == numeroDeMedidor) {
                medidores.get(i).setNumeroDeMedidor(nuevoNumeroDeMedidor);
            } else {
                if(i>=medidores.size())
                    throw new ExcepcionMedidorNoExist();
            }
        }
    }


}