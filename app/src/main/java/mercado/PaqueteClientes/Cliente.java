package mercado.PaqueteClientes;

import java.util.ArrayList;

import mercado.PaqueteContrato.Contrato;
import mercado.PaqueteContrato.ExcepcionesContratos.ContratoAlreadyExistsException;
import mercado.PaqueteContrato.ExcepcionesContratos.ContratoDoesntExistsException;

public abstract class Cliente {

    private String nombre;
    private String direccion;
    private Integer telefono;
    private String email;
    private ArrayList<Contrato> contratosFirmadosConElMercado;


    public Cliente(String nombre, String direccion, Integer telefono,
            String email) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        contratosFirmadosConElMercado = new ArrayList<Contrato>();

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public abstract String getCuit();

    public ArrayList<Contrato> getContratosFirmados(){
        return contratosFirmadosConElMercado;
    }
    
    public Contrato getContrato(Integer numeroContrato) throws ContratoDoesntExistsException{
        Contrato contratoEncontrado = null;
        for(Contrato var: contratosFirmadosConElMercado){
            if(var.getNumeroContrato().equals(numeroContrato)){
                contratoEncontrado = var;
            }
        }
        if(contratoEncontrado == null){
            throw new ContratoDoesntExistsException();
        }
        return contratoEncontrado;
    }

    public int cantidadDeContratosFirmados(){
        return contratosFirmadosConElMercado.size();
    }
    
    public void agregarContrato(Contrato contrato) throws ContratoAlreadyExistsException {
        for (Contrato c : contratosFirmadosConElMercado) {
            if (c.getNumeroContrato().equals(contrato.getNumeroContrato())) {
                throw new ContratoAlreadyExistsException();
            }
        }
        contratosFirmadosConElMercado.add(contrato);
    }

    public void actualizarContrato(Integer numeroContrato, Contrato contratoActualizado) throws ContratoDoesntExistsException {
        Contrato contrato = getContrato(numeroContrato);
        contrato.setFecha_inicio(contratoActualizado.getFecha_inicio());
        contrato.setFecha_fin(contratoActualizado.getFecha_fin());
        contrato.setMonto_mensual(contratoActualizado.getMonto_mensual());
        contrato.setResponsable_mercado(contratoActualizado.getResponsable_mercado());
        contrato.setPuesto(contratoActualizado.getPuesto());
        contrato.setCliente(contratoActualizado.getCliente());
    }

    public void eliminarContrato(Integer numeroContrato) throws ContratoDoesntExistsException {
        Contrato contrato = getContrato(numeroContrato);
        contratosFirmadosConElMercado.remove(contrato);
    }

    @Override
    public boolean equals(Object c){
        Cliente clienteComparar = (Cliente)c;
        if(this.getCuit().equals(clienteComparar.getCuit())){
            return true;
        }
        else{
            return false;
        }
    }

    
}