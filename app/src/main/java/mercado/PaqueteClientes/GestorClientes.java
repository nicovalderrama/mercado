package mercado.PaqueteClientes;

import java.util.ArrayList;
import java.util.List;

public class GestorClientes {
        private List<Cliente> clientes;

    public GestorClientes() {
        this.clientes = new ArrayList<>();
    }

    public void agregarCliente(Cliente cliente) throws ClienteYaExisteException {
        if (clientes.contains(cliente)) {
            throw new ClienteYaExisteException(cliente.getCuit());
        }
        clientes.add(cliente);
    }

    public Cliente obtenerCliente(String cuit) throws ClienteInexistenteException {
        for (Cliente cliente : clientes) {
            if (cliente.getCuit().equals(cuit)) {
                return cliente;
            }
        }
        throw new ClienteInexistenteException(cuit);
    }

    public void actualizarClienteQuintero(String cuit, ClienteQuintero clienteActualizado) throws ClienteInexistenteException {
        Cliente cliente = obtenerCliente(cuit);
        if (cliente instanceof ClienteQuintero) {
            ClienteQuintero clienteQuintero = (ClienteQuintero) cliente;
            clienteQuintero.setNombre(clienteActualizado.getNombre());
            clienteQuintero.setApellido(clienteActualizado.getApellido());
            clienteQuintero.setDireccion(clienteActualizado.getDireccion());
            clienteQuintero.setTelefono(clienteActualizado.getTelefono());
            clienteQuintero.setEmail(clienteActualizado.getEmail());
            clienteQuintero.setCuit(clienteActualizado.getCuit());
        } else {
            throw new ClienteInexistenteException(cuit);
        }
    }

    public void actualizarClienteEmpresa(String cuit, ClienteEmpresa clienteActualizado) throws ClienteInexistenteException {
        Cliente cliente = obtenerCliente(cuit);
        if (cliente instanceof ClienteEmpresa) {
            ClienteEmpresa clienteEmpresa = (ClienteEmpresa) cliente;
            clienteEmpresa.setNombre(clienteActualizado.getNombre());
            clienteEmpresa.setDireccion(clienteActualizado.getDireccion());
            clienteEmpresa.setTelefono(clienteActualizado.getTelefono());
            clienteEmpresa.setEmail(clienteActualizado.getEmail());
            clienteEmpresa.setCuit(clienteActualizado.getCuit());
        } else {
            throw new ClienteInexistenteException(cuit);
        }
    }

    public void eliminarCliente(String cuit) throws ClienteInexistenteException {
        Cliente cliente = obtenerCliente(cuit);
        clientes.remove(cliente);
    }

    public List<Cliente> obtenerTodosLosClientes() {
        return new ArrayList<>(clientes);
    }
}
