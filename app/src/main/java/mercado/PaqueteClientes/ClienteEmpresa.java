package mercado.PaqueteClientes;

public class ClienteEmpresa extends Cliente {

    private String cuit;
    public ClienteEmpresa(String nombre, String direccion, Integer telefono,
            String email, String cuit) {
        super(nombre, direccion, telefono, email);
        this.cuit= cuit;
    }
    public String getCuit() {
        return cuit;
    }
    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    
}