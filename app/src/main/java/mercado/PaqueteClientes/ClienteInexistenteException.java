package mercado.PaqueteClientes;

public class ClienteInexistenteException extends Exception {
    public ClienteInexistenteException(String cuit) {
        super("Cliente con CUIT " + cuit + " no existe.");
    }
}
