package mercado.PaqueteClientes;


public class ClienteQuintero extends Cliente {

    private String apellido;
    private String cuil;


    public ClienteQuintero(String nombre, String apellido, String direccion, Integer telefono, String email, String cuil) {
        super(nombre, direccion, telefono, email);
        this.apellido= apellido;
        this.cuil= cuil;
    }


    public String getApellido() {
        return apellido;
    }


    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


    public String getCuit() {
        return cuil;
    }


    public void setCuit(String cuil) {
        this.cuil = cuil;
    }

    
}