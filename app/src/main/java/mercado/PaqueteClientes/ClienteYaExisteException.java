package mercado.PaqueteClientes;

public class ClienteYaExisteException extends Exception{
    public ClienteYaExisteException(String cuit) {
        super("Cliente con CUIT " + cuit + " ya existe.");
    }
}
