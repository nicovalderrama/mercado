package mercado.PaqueteSectores.ExcepcionesSectores;

public class SectorDoesntExistsException extends RuntimeException {

    public SectorDoesntExistsException() {
        super("El sector no existe");
    }
}
