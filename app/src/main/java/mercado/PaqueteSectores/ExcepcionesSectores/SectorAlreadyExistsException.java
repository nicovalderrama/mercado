package mercado.PaqueteSectores.ExcepcionesSectores;

public class SectorAlreadyExistsException extends RuntimeException {

    public SectorAlreadyExistsException() {
        super("El sector ya existe");
    }
}
