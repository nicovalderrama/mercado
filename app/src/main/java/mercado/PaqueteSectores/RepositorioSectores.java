package mercado.PaqueteSectores;

import java.util.ArrayList;

import mercado.PaqueteSectores.ExcepcionesSectores.SectorAlreadyExistsException;
import mercado.PaqueteSectores.ExcepcionesSectores.SectorDoesntExistsException;

public class RepositorioSectores {
    private ArrayList<Sector> sectores;

    public RepositorioSectores() {
        this.sectores = new ArrayList<>();
    }

    public ArrayList<Sector> getSectores() {
        return sectores;
    }

    public void setSectores(ArrayList<Sector> sectores) {
        this.sectores = sectores;
    }

    public void agregarSector(Sector sector) throws SectorAlreadyExistsException {
        for (Sector sec : sectores) {
            if (sec.equals(sector)) {
                throw new SectorAlreadyExistsException();
            }
        }
        sectores.add(sector);
    }

    public Sector buscarSector(Sector sector) throws SectorDoesntExistsException {
        for (Sector sec : sectores) {
            if (sec.equals(sector)) {
                return sec;
            }
        }
        throw new SectorDoesntExistsException();
    }

    public void eliminarSector(Sector sector) throws SectorDoesntExistsException {
        if (!sectores.contains(sector)) {
            throw new SectorDoesntExistsException();
        }
        sectores.remove(sector);
    }

}
