package mercado.PaqueteSectores;

import java.util.ArrayList;

import mercado.PaqueteSectores.ExcepcionesPuestos.PuestoAlreadyExistsException;
import mercado.PaqueteSectores.ExcepcionesPuestos.PuestoDoesntExistsException;

public class RepositorioPuestos {
    private ArrayList<Puesto> puestos;

    public RepositorioPuestos() {
        this.puestos = new ArrayList<>();
    }

    public ArrayList<Puesto> getPuestos() {
        return puestos;
    }

    public void setPuestos(ArrayList<Puesto> puestos) {
        this.puestos = puestos;
    }

    public void agregarPuesto(Puesto puesto) throws PuestoAlreadyExistsException {
        for (Puesto pues : puestos) {
            if (pues.equals(puesto)) {
                throw new PuestoAlreadyExistsException();
            }
        }
        puestos.add(puesto);
    }

    public Puesto buscarPuesto(Puesto puesto) throws PuestoDoesntExistsException {
        for (Puesto pues : puestos) {
            if (pues.equals(puesto)) {
                return pues;
            }
        }
        throw new PuestoDoesntExistsException();
    }

    public void eliminarPuesto(Puesto puesto) throws PuestoDoesntExistsException {
        if (!puestos.remove(puesto)) {
            throw new PuestoDoesntExistsException();
        }
    }

}
