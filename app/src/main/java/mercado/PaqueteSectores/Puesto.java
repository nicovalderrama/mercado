package mercado.PaqueteSectores;

import mercado.PaqueteLecturas.Medidor;

public class Puesto {
    private int num_puesto;
    private double precio_alquiler;
    private boolean esta_disponible;
    private String dimensiones;
    private boolean tieneTecho;
    private boolean tieneCamara;
    private Medidor medidor;

    public Puesto(int num_puesto, double precio_alquiler, boolean esta_disponible, String dimensiones,
            boolean tieneTecho, boolean tieneCamara, Medidor medidor) {
        this.num_puesto = num_puesto;
        this.precio_alquiler = precio_alquiler;
        this.esta_disponible = esta_disponible;
        this.dimensiones = dimensiones;
        this.tieneTecho = tieneTecho;
        this.tieneCamara = tieneCamara;
        this.medidor = medidor;
    }

    public int getNum_puesto() {
        return num_puesto;
    }

    public void setNum_puesto(int num_puesto) {
        this.num_puesto = num_puesto;
    }

    public double getPrecio_alquiler() {
        return precio_alquiler;
    }

    public void setPrecio_alquiler(double precio_alquiler) {
        this.precio_alquiler = precio_alquiler;
    }

    public boolean isEsta_disponible() {
        return esta_disponible;
    }

    public void setEsta_disponible(boolean esta_disponible) {
        this.esta_disponible = esta_disponible;
    }

    public String getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }

    public boolean isTieneTecho() {
        return tieneTecho;
    }

    public void setTieneTecho(boolean tieneTecho) {
        this.tieneTecho = tieneTecho;
    }

    public boolean isTieneCamara() {
        return tieneCamara;
    }

    public void setTieneCamara(boolean tieneCamara) {
        this.tieneCamara = tieneCamara;
    }

    public Medidor getMedidor() {
        return medidor;
    }

    public void setMedidor(Medidor medidor) {
        this.medidor = medidor;
    }

    @Override
    public boolean equals(Object obj) {
        Puesto puesto = (Puesto) obj;
        return num_puesto == puesto.getNum_puesto();
    }

}
