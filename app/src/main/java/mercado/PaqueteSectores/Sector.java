package mercado.PaqueteSectores;

import java.util.ArrayList;

public class Sector {
    private int num_sector;
    private String nombre_sector;
    private String ubicacion_sector;
    private ArrayList<Puesto> puestos;

    public Sector(int num_sector, String nombre_sector, String ubicacion_sector) {
        this.num_sector = num_sector;
        this.nombre_sector = nombre_sector;
        this.ubicacion_sector = ubicacion_sector;
    }

    public int getNum_sector() {
        return num_sector;
    }

    public void setNum_sector(int num_sector) {
        this.num_sector = num_sector;
    }

    public String getNombre_sector() {
        return nombre_sector;
    }

    public void setNombre_sector(String nombre_sector) {
        this.nombre_sector = nombre_sector;
    }

    public String getUbicacion_sector() {
        return ubicacion_sector;
    }

    public void setUbicacion_sector(String ubicacion_sector) {
        this.ubicacion_sector = ubicacion_sector;
    }

    public ArrayList<Puesto> getPuestos() {
        return puestos;
    }

    public void setPuestos(ArrayList<Puesto> puestos) {
        this.puestos = puestos;
    }

    @Override
    public boolean equals(Object obj) {
        Sector sector = (Sector) obj;
        return nombre_sector.equals(sector.getNombre_sector());
    }

}
