package mercado.PaqueteSectores.ExcepcionesPuestos;

public class PuestoDoesntExistsException extends RuntimeException {
    public PuestoDoesntExistsException() {
        super("El puesto no existe");
    }
}
