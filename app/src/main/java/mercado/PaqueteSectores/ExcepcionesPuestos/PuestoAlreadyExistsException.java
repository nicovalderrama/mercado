package mercado.PaqueteSectores.ExcepcionesPuestos;

public class PuestoAlreadyExistsException extends RuntimeException {
    public PuestoAlreadyExistsException() {
        super("El puesto ya existe");
    }
}
