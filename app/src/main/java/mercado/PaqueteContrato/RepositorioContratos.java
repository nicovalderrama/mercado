package mercado.PaqueteContrato;

import java.util.ArrayList;

import mercado.PaqueteContrato.ExcepcionesContratos.ContratoAlreadyExistsException;
import mercado.PaqueteContrato.ExcepcionesContratos.ContratoDoesntExistsException;

public class RepositorioContratos {
    private ArrayList<Contrato> contratos;

    public ArrayList<Contrato> getContratos() {
        return contratos;
    }

    public RepositorioContratos() {
        this.contratos = new ArrayList<>();
    }

    public void agregarContrato(Contrato contrato) throws ContratoAlreadyExistsException {
        for (Contrato cont : contratos) {
            if (cont.equals(contrato)) {
                throw new ContratoAlreadyExistsException();
            }
        }
        contratos.add(contrato);
    }

    public Contrato buscarContrato(Contrato contrato) throws ContratoDoesntExistsException {
        for (Contrato cont : contratos) {
            if (cont.equals(contrato)) {
                return cont;
            }
        }
        throw new ContratoDoesntExistsException();
    }

    public void eliminarContrato(Contrato contrato) throws ContratoDoesntExistsException {
        if (!contratos.contains(contrato)) {
            throw new ContratoDoesntExistsException();
        }
        contratos.remove(contrato);
    }
}
