package mercado.PaqueteContrato;

import java.time.LocalDate;

import mercado.PaqueteClientes.Cliente;
import mercado.PaqueteSectores.Puesto;

public class Contrato {
    private Integer numeroContrato;
    private LocalDate fecha_inicio;
    private LocalDate fecha_fin;
    private double monto_mensual;
    private ResponsableMercado responsable_mercado;
    private Puesto puesto;
    private Cliente cliente;

    public Contrato(Integer numeroContrato, LocalDate fecha_inicio, LocalDate fecha_fin, double monto_mensual,
            String responsable_registro, ResponsableMercado responsable_mercado, Puesto puesto, Cliente cliente) {
        this.numeroContrato = numeroContrato;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.monto_mensual = monto_mensual;
        this.responsable_mercado = responsable_mercado;
        this.puesto = puesto;
        this.cliente = cliente;
    }

    public Integer getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Integer numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public LocalDate getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(LocalDate fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public LocalDate getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(LocalDate fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public double getMonto_mensual() {
        return monto_mensual;
    }

    public void setMonto_mensual(double monto_mensual) {
        this.monto_mensual = monto_mensual;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ResponsableMercado getResponsable_mercado() {
        return responsable_mercado;
    }

    public void setResponsable_mercado(ResponsableMercado responsable_mercado) {
        this.responsable_mercado = responsable_mercado;
    }

    @Override
    public String toString() {
        return "Contrato{" +
                "numeroContrato=" + numeroContrato +
                ", fecha_inicio=" + fecha_inicio +
                ", fecha_fin=" + fecha_fin +
                ", monto_mensual=" + monto_mensual +
                ", responsable_mercado=" + responsable_mercado +
                ", puesto=" + puesto +
                ", cliente=" + cliente +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        Contrato contrato = (Contrato) o;
        return this.getNumeroContrato().equals(contrato.getNumeroContrato());
    }
}
