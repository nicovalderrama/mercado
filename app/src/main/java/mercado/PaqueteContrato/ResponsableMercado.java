package mercado.PaqueteContrato;

public class ResponsableMercado {

    private String cuit;
    private String nombre;
    private String apellido;

    public ResponsableMercado(String cuit, String nombre, String apellido) {
        this.cuit = cuit;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

}