package mercado.PaqueteContrato.ExcepcionesContratos;

public class ContratoAlreadyExistsException extends RuntimeException {

    public ContratoAlreadyExistsException() {
        super("El contrato ya existe");
    }

}
