package mercado.PaqueteContrato.ExcepcionesContratos;

public class ContratoDoesntExistsException extends RuntimeException {
    public ContratoDoesntExistsException() {
        super("El contrato no existe");
    }
}
