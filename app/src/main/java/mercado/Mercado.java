package mercado;

import mercado.PaqueteClientes.GestorClientes;
import mercado.PaqueteContrato.RepositorioContratos;
import mercado.PaqueteLecturas.RepositorioMedidores;
import mercado.PaqueteSectores.RepositorioPuestos;
import mercado.PaqueteSectores.RepositorioSectores;

public class Mercado {

    private static Mercado instancia;
    private GestorClientes clientes = new GestorClientes();
    private RepositorioContratos contratos = new RepositorioContratos();
    private RepositorioMedidores medidores = new RepositorioMedidores();
    private RepositorioSectores sectores = new RepositorioSectores();
    private RepositorioPuestos puestos = new RepositorioPuestos();

    private Mercado(){
    }

    public static Mercado getInstance(){
        if (instancia == null) {
            instancia = new Mercado();
        }
        return instancia;
    }
    
    public GestorClientes getClientes() {
        return clientes;
    }

    public RepositorioContratos getContratos() {
        return contratos;
    }

    public RepositorioMedidores getMedidores() {
        return medidores;
    }

    public RepositorioSectores getSectores() {
        return sectores;
    }

    public RepositorioPuestos getPuestos() {
        return puestos;
    }

    
}
