package mercado.TestsSectores;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;
import mercado.PaqueteSectores.Puesto;
import mercado.PaqueteSectores.RepositorioPuestos;
import mercado.PaqueteSectores.ExcepcionesPuestos.PuestoAlreadyExistsException;
import mercado.PaqueteSectores.ExcepcionesPuestos.PuestoDoesntExistsException;

public class RepositorioPuestosTest {
    @Test
    public void añadirPuesto() throws PuestoAlreadyExistsException {
        RepositorioPuestos repositorio = new RepositorioPuestos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        repositorio.agregarPuesto(puesto);
        assertEquals(puesto, repositorio.getPuestos().get(0));
    }

    @Test
    public void eliminarPuesto() throws PuestoDoesntExistsException, PuestoAlreadyExistsException {
        RepositorioPuestos repositorio = new RepositorioPuestos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        repositorio.agregarPuesto(puesto);
        repositorio.eliminarPuesto(puesto);
        assertTrue(repositorio.getPuestos().isEmpty());
    }

    @Test
    public void buscarPuesto() throws PuestoDoesntExistsException, PuestoAlreadyExistsException {
        RepositorioPuestos repositorio = new RepositorioPuestos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        repositorio.agregarPuesto(puesto);
        Puesto puestoEncontrado = repositorio.buscarPuesto(puesto);
        assertEquals(puesto, puestoEncontrado);
    }

    @Test
    public void añadirPuestoExistente() throws PuestoAlreadyExistsException {
        RepositorioPuestos repositorio = new RepositorioPuestos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        repositorio.agregarPuesto(puesto);
        repositorio.agregarPuesto(puesto);
    }

    @Test
    public void buscarPuestoNoExistente() throws PuestoDoesntExistsException {
        RepositorioPuestos repositorio = new RepositorioPuestos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        repositorio.buscarPuesto(puesto);
    }

    @Test
    public void eliminarPuestoNoExistente() throws PuestoDoesntExistsException {
        RepositorioPuestos repositorio = new RepositorioPuestos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        repositorio.eliminarPuesto(puesto);
    }
}
