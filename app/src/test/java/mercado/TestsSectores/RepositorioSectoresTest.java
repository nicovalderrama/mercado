package mercado.TestsSectores;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.PaqueteSectores.RepositorioSectores;
import mercado.PaqueteSectores.Sector;
import mercado.PaqueteSectores.ExcepcionesSectores.SectorAlreadyExistsException;
import mercado.PaqueteSectores.ExcepcionesSectores.SectorDoesntExistsException;

public class RepositorioSectoresTest {
    @Test
    public void agregarSector() throws SectorAlreadyExistsException {
        RepositorioSectores sectores = new RepositorioSectores();
        Sector sector = new Sector(1, "Bebidas", null);
        sectores.agregarSector(sector);
        assertEquals(sector, sectores.getSectores().get(0));
    }

    @Test
    public void eliminarSector() throws SectorDoesntExistsException, SectorAlreadyExistsException {
        RepositorioSectores sectores = new RepositorioSectores();
        Sector sector = new Sector(1, "Bebidas", null);
        sectores.agregarSector(sector);
        sectores.eliminarSector(sector);
        assertEquals(0, sectores.getSectores().size());
    }

    @Test
    public void buscarSector() throws SectorDoesntExistsException, SectorAlreadyExistsException {
        RepositorioSectores sectores = new RepositorioSectores();
        Sector sector = new Sector(1, "Bebidas", null);
        sectores.agregarSector(sector);
        Sector sectorEncontrado = sectores.buscarSector(sector);
        assertEquals(sector, sectorEncontrado);
    }

}
