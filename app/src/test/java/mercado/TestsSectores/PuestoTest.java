package mercado.TestsSectores;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;

import org.junit.Test;

import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;
import mercado.PaqueteSectores.Puesto;

public class PuestoTest {
    @Test
    public void comprobarEquals_PuestosIguales() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(500, lecturas);
        Puesto puesto = new Puesto(5, 300, true, "30m2", true, false, medidor);
        Puesto puesto1 = new Puesto(5, 300, true, "30m2", true, false, medidor);

        boolean comprobacion = puesto.equals(puesto1);
        assertTrue(comprobacion);
    }

    @Test
    public void comprobarEquals_PuestosDiferentes() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(500, lecturas);
        Puesto puesto = new Puesto(5, 300, true, "30m2", true, false, medidor);
        Puesto puesto1 = new Puesto(10, 300, true, "30m2", true, false, medidor);

        boolean comprobacion = puesto.equals(puesto1);
        assertFalse(comprobacion);
    }
}
