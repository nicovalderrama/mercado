package mercado.TestsLecturas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Test;

import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;

import java.time.LocalDate;

public class MedidorTest {

    @Test
    public void creacionDeMedidor() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor1 = new Medidor(19, lecturas);

        Integer numeroDeMedidor1 = medidor1.getNumeroDeMedidor();

        assertEquals(Integer.valueOf(19), numeroDeMedidor1);
    }

    @Test
    public void elMedidorYaExiste() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor1 = new Medidor(10, lecturas);
        Medidor medidor2 = new Medidor(10, lecturas);

        assertTrue(medidor2.equals(medidor1));
    }

    @Test
    public void elMedidorNoExiste() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor1 = new Medidor(10, lecturas);
        Medidor medidor2 = new Medidor(11, lecturas);

        assertFalse(medidor2.equals(medidor1));
    }

    @Test
    public void agregarLecturasAlArray() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor1 = new Medidor(10, lecturas);
        Lectura lectura1 = new Lectura(12, LocalDate.parse("2024-05-17"), medidor1);

        medidor1.addLectura(lectura1);
        ArrayList<Lectura> prueba = medidor1.getLecturas();
        int tamanoArray = prueba.size();

        assertEquals(1, tamanoArray);
    }

}
