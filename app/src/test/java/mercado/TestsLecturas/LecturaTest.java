package mercado.TestsLecturas;

import org.junit.Test;

import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.time.LocalDate;
import java.util.ArrayList;

public class LecturaTest {

    @Test
    public void laFechaYaExiste() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor1 = new Medidor(12, lecturas);
        Lectura lectura1 = new Lectura(14, LocalDate.parse("2024-05-17"), medidor1);
        Lectura lectura2 = new Lectura(14, LocalDate.parse("2024-05-17"), medidor1);

        boolean comprobacion = lectura1.equals(lectura2);
        assertTrue(comprobacion);
    }

    @Test
    public void laFechaNoExiste() {
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor1 = new Medidor(12, lecturas);
        Lectura lectura1 = new Lectura(14, LocalDate.parse("2024-05-12"), medidor1);
        Lectura lectura2 = new Lectura(14, LocalDate.parse("2024-05-17"), medidor1);

        boolean comprobacion = lectura1.equals(lectura2);
        assertFalse(comprobacion);
    }
}
