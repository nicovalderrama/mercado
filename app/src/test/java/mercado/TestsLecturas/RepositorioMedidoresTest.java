package mercado.TestsLecturas;

import static org.junit.Assert.assertEquals;

/*import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
*/
import java.util.ArrayList;
import org.junit.Test;
//import java.time.LocalDate;

import mercado.PaqueteLecturas.ExcepcionesLecturas.*;
import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;
import mercado.PaqueteLecturas.RepositorioMedidores;

public class RepositorioMedidoresTest {

    @Test (expected = ExcepcionRepositorioDeMedidoresVacia.class )
    public void verificarSiElRepositorioEstaVacio() throws ExcepcionRepositorioDeMedidoresVacia
    {
        RepositorioMedidores repo = new RepositorioMedidores();
        repo.getMedidores();
    }

    @Test (expected = ExcepcionMedidorSiExiste.class)
    public void verificarElMedidorExiste()throws ExcepcionMedidorSiExiste {
        ArrayList<Lectura> lecturas = new ArrayList<>(); 
        RepositorioMedidores repo = new RepositorioMedidores();

        Medidor medidor1 = new Medidor(1, lecturas);
        Medidor medidor2 = new Medidor(2, lecturas);
        Medidor medidor3 = new Medidor(1, lecturas);

        repo.agregarMedidor(medidor1);
        repo.agregarMedidor(medidor2); 
        repo.agregarMedidor(medidor3);
          
    }

    @Test(expected = ExcepcionMedidorNoExist.class)
    public void verificarElMedidorNoExiste() throws ExcepcionMedidorNoExist {

        ArrayList<Lectura> lecturas = new ArrayList<>(); 
        RepositorioMedidores repo = new RepositorioMedidores();

        Medidor medidor1 = new Medidor(1, lecturas);
        Medidor medidor2 = new Medidor(2, lecturas);

        repo.agregarMedidor(medidor1);
        repo.agregarMedidor(medidor2); 
        repo.eliminarMedidor(3); 
    }

    @Test
    public void agregarMedidor()
    {
        ArrayList<Lectura> lecturas = new ArrayList<>(); 
        RepositorioMedidores repo = new RepositorioMedidores();

        Medidor medidor1 = new Medidor(1, lecturas);
        Medidor medidor2 = new Medidor(2, lecturas);

        repo.agregarMedidor(medidor1);
        repo.agregarMedidor(medidor2); 
        try{
            assertEquals(Integer.valueOf(2), Integer.valueOf(repo.getMedidores().size()));
        }catch(ExcepcionRepositorioDeMedidoresVacia e){}
  }

    @Test
    public void eliminarMedidor()
    {
        ArrayList<Lectura> lecturas = new ArrayList<>(); 
        RepositorioMedidores repo = new RepositorioMedidores();

        Medidor medidor1 = new Medidor(1, lecturas);
        Medidor medidor2 = new Medidor(2, lecturas);

        repo.agregarMedidor(medidor1);
        repo.agregarMedidor(medidor2); 
        repo.eliminarMedidor(1); 

        try{
            assertEquals(Integer.valueOf(1), Integer.valueOf(repo.getMedidores().size()));
        } catch( ExcepcionRepositorioDeMedidoresVacia e){}    
    }

    @Test 
    public void modificarMedidor()
    {
        ArrayList<Lectura> lecturas = new ArrayList<>(); 
        RepositorioMedidores repo = new RepositorioMedidores();

        Medidor medidor1 = new Medidor(1, lecturas);

        repo.agregarMedidor(medidor1);

        repo.modificarMedidor(1, 23);
        int i = 0;
        ArrayList<Medidor>actual = new ArrayList<>();
        try{
        actual = repo.getMedidores();
        } catch(ExcepcionRepositorioDeMedidoresVacia e)
        {}


        assertEquals(Integer.valueOf(23), Integer.valueOf(actual.get(i).getNumeroDeMedidor()));

    }


}
