package mercado.TestClientes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import mercado.PaqueteClientes.*;

public class GestorClientesTest {
    private GestorClientes gestorClientes;

    @Before
    public void setUp() {
        gestorClientes = new GestorClientes();
    }

    @Test
    public void testAgregarCliente() throws ClienteYaExisteException, ClienteInexistenteException {
        Cliente cliente = new ClienteQuintero("Juan", "Perez", "Calle Falsa 123", 12345678, "juan@example.com", "20-12345678-9");
        gestorClientes.agregarCliente(cliente);
        assertEquals(cliente, gestorClientes.obtenerCliente("20-12345678-9"));
    }

    @Test
    public void testAgregarCliente_ClienteYaExisteException() {
        Cliente cliente = new ClienteQuintero("Juan", "Perez", "Calle Falsa 123", 12345678, "juan@example.com", "20-12345678-9");
        assertThrows(ClienteYaExisteException.class, () -> {
            gestorClientes.agregarCliente(cliente);
            gestorClientes.agregarCliente(cliente);
        });
    }

    @Test
    public void testObtenerCliente() throws ClienteInexistenteException, ClienteYaExisteException {
        Cliente cliente = new ClienteQuintero("Juan", "Perez", "Calle Falsa 123", 12345678, "juan@example.com", "20-12345678-9");
        gestorClientes.agregarCliente(cliente);
        Cliente clienteObtenido = gestorClientes.obtenerCliente("20-12345678-9");
        assertEquals(cliente, clienteObtenido);
    }

    @Test
    public void testObtenerCliente_ClienteInexistenteException() {
        assertThrows(ClienteInexistenteException.class, () -> {
            gestorClientes.obtenerCliente("20-00000000-0");
        });
    }

    @Test
    public void testActualizarClienteQuintero() throws ClienteYaExisteException, ClienteInexistenteException {
        ClienteQuintero cliente = new ClienteQuintero("Juan", "Perez", "Calle Falsa 123", 12345678, "juan@example.com", "20-12345678-9");
        gestorClientes.agregarCliente(cliente);
        ClienteQuintero clienteActualizado = new ClienteQuintero("Juan", "Perez", "Calle Verdadera 456", 87654321, "juan.perez@example.com", "20-12345678-9");
        gestorClientes.actualizarClienteQuintero("20-12345678-9", clienteActualizado);
        ClienteQuintero clienteObtenido = (ClienteQuintero) gestorClientes.obtenerCliente("20-12345678-9");
        assertEquals(clienteActualizado.getDireccion(), clienteObtenido.getDireccion());
        assertEquals(clienteActualizado.getTelefono(), clienteObtenido.getTelefono());
        assertEquals(clienteActualizado.getEmail(), clienteObtenido.getEmail());
    }

    @Test
    public void testActualizarClienteQuintero_ClienteInexistenteException() {
        ClienteQuintero clienteActualizado = new ClienteQuintero("Juan", "Perez", "Calle Verdadera 456", 87654321, "juan.perez@example.com", "20-00000000-0");
        assertThrows(ClienteInexistenteException.class, () -> {
            gestorClientes.actualizarClienteQuintero("20-00000000-0", clienteActualizado);
        });
    }


    @Test
    public void testActualizarClienteEmpresa() throws ClienteYaExisteException, ClienteInexistenteException {
        ClienteEmpresa cliente = new ClienteEmpresa("Empresa SA", "Calle Empresa 123", 12345678, "empresa@example.com", "30-12345678-9");
        gestorClientes.agregarCliente(cliente);
        ClienteEmpresa clienteActualizado = new ClienteEmpresa("Empresa SA", "Calle Empresa 456", 87654321, "empresa.actualizada@example.com", "30-12345678-9");
        gestorClientes.actualizarClienteEmpresa("30-12345678-9", clienteActualizado);
        ClienteEmpresa clienteObtenido = (ClienteEmpresa) gestorClientes.obtenerCliente("30-12345678-9");
        assertEquals(clienteActualizado.getDireccion(), clienteObtenido.getDireccion());
        assertEquals(clienteActualizado.getTelefono(), clienteObtenido.getTelefono());
        assertEquals(clienteActualizado.getEmail(), clienteObtenido.getEmail());
    }

    @Test
    public void testActualizarClienteEmpresa_ClienteInexistenteException() {
        ClienteEmpresa clienteActualizado = new ClienteEmpresa("Empresa SA", "Calle Empresa 456", 87654321, "empresa.actualizada@example.com", "30-00000000-0");
        assertThrows(ClienteInexistenteException.class, () -> {
            gestorClientes.actualizarClienteEmpresa("30-00000000-0", clienteActualizado);
        });
    }


    @Test
    public void testEliminarCliente() throws ClienteYaExisteException, ClienteInexistenteException {
        Cliente cliente = new ClienteQuintero("Juan", "Perez", "Calle Falsa 123", 12345678, "juan@example.com", "20-12345678-9");
        gestorClientes.agregarCliente(cliente);
        gestorClientes.eliminarCliente("20-12345678-9");
        assertThrows(ClienteInexistenteException.class, () -> {
            gestorClientes.obtenerCliente("20-12345678-9");
        });
    }

    @Test
    public void testEliminarCliente_ClienteInexistenteException() {
        assertThrows(ClienteInexistenteException.class, () -> {
            gestorClientes.eliminarCliente("20-00000000-0");
        });
    }

    @Test
    public void testObtenerTodosLosClientes() throws ClienteYaExisteException {
        Cliente cliente1 = new ClienteQuintero("Juan", "Perez", "Calle Falsa 123", 12345678, "juan@example.com", "20-12345678-9");
        Cliente cliente2 = new ClienteEmpresa("Empresa SA", "Calle Empresa 123", 12345678, "empresa@example.com", "30-12345678-9");
        gestorClientes.agregarCliente(cliente1);
        gestorClientes.agregarCliente(cliente2);
        List<Cliente> todosLosClientes = gestorClientes.obtenerTodosLosClientes();
        assertEquals(2, todosLosClientes.size());
        assertTrue(todosLosClientes.contains(cliente1));
        assertTrue(todosLosClientes.contains(cliente2));
    }

    @Test
    public void testObtenerTodosLosClientes_SinClientes() {
        List<Cliente> todosLosClientes = gestorClientes.obtenerTodosLosClientes();
        assertEquals(0, todosLosClientes.size());
    }
}
