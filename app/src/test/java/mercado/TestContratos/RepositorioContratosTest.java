package mercado.TestContratos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

import mercado.PaqueteClientes.ClienteEmpresa;
import mercado.PaqueteClientes.ClienteQuintero;
import mercado.PaqueteContrato.Contrato;
import mercado.PaqueteContrato.RepositorioContratos;
import mercado.PaqueteContrato.ResponsableMercado;
import mercado.PaqueteContrato.ExcepcionesContratos.ContratoAlreadyExistsException;
import mercado.PaqueteContrato.ExcepcionesContratos.ContratoDoesntExistsException;
import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;
import mercado.PaqueteSectores.Puesto;

/**
 * RepositorioContratosTest
 */
public class RepositorioContratosTest {
    @Test
    public void agregarContrato() throws ContratoAlreadyExistsException {
        RepositorioContratos repositorio = new RepositorioContratos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        ClienteQuintero cliente = new ClienteQuintero("JORGE", "PEREZ", "Direccion1", 123456789,
                "jorgeperz@gmail.com", "27456321547");

        Contrato contrato = new Contrato(1, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                "registro1", new ResponsableMercado("204778211", "Marcos", "Ramirez"), puesto, cliente);

        repositorio.agregarContrato(contrato);

        assertEquals(1, repositorio.getContratos().size());
    }

    @Test
    public void buscarContratoExistente() throws ContratoDoesntExistsException, ContratoAlreadyExistsException {
        RepositorioContratos repositorio = new RepositorioContratos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        ClienteQuintero cliente = new ClienteQuintero("JORGE", "PEREZ", "Direccion1", 123456789,
                "jorgeperz@gmail.com", "27456321547");

        Contrato contrato = new Contrato(1, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                "registro1", new ResponsableMercado("204778211", "Marcos", "Ramirez"), puesto, cliente);
        repositorio.agregarContrato(contrato);

        Contrato contratoEncontrado = repositorio.buscarContrato(contrato);

        assertEquals(contrato, contratoEncontrado);
    }

    @Test
    public void eliminarContratoExistente() throws ContratoDoesntExistsException, ContratoAlreadyExistsException {
        RepositorioContratos repositorio = new RepositorioContratos();
        ArrayList<Lectura> lecturas = new ArrayList<>();
        Medidor medidor = new Medidor(19, lecturas);
        Puesto puesto = new Puesto(1, 500.0, true, "10x10", true, true, medidor);
        ClienteEmpresa cliente = new ClienteEmpresa("Cliente1", "Direccion1", 123456789, "email1@ejemplo.com",
                "20451116281");
        Contrato contrato = new Contrato(1, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                "registro1", new ResponsableMercado("204778211", "Marcos", "Ramirez"), puesto, cliente);
        repositorio.agregarContrato(contrato);

        repositorio.eliminarContrato(contrato);
        assertFalse(repositorio.getContratos().contains(contrato));
    }
}