package mercado.TestContratos;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

import mercado.PaqueteClientes.ClienteEmpresa;
import mercado.PaqueteContrato.Contrato;
import mercado.PaqueteContrato.ResponsableMercado;
import mercado.PaqueteLecturas.Lectura;
import mercado.PaqueteLecturas.Medidor;
import mercado.PaqueteSectores.Puesto;

@SuppressWarnings("unused")
public class ContratoTest {

        @Test
        public void comprobarEquals_ContratosIguales() {
                // contrato 1
                ArrayList<Lectura> lecturas = new ArrayList<>();
                Medidor medidor1 = new Medidor(19, lecturas);
                Lectura lectura1 = new Lectura(500, LocalDate.of(2023, 1, 1), medidor1);
                medidor1.addLectura(lectura1);
                ClienteEmpresa cliente1 = new ClienteEmpresa("Cliente1", "Direccion1", 123456789, "email1@ejemplo.com",
                                "2045112781");
                Puesto puesto1 = new Puesto(1, 1000.0, true, "10x10", true, false, medidor1);
                ResponsableMercado responsable1 = new ResponsableMercado("204778211", "Marcos", "Ramirez");

                // contrato2
                ArrayList<Lectura> lecturas2 = new ArrayList<>();
                Medidor medidor2 = new Medidor(19, lecturas2);
                Lectura lectura2 = new Lectura(500, LocalDate.of(2023, 1, 1), medidor2);
                medidor2.addLectura(lectura2);
                ClienteEmpresa cliente2 = new ClienteEmpresa("Cliente1", "Direccion1", 123456789, "email1@ejemplo.com",
                                "2045112781");
                Puesto puesto2 = new Puesto(1, 1000.0, true, "10x10", true, false, medidor2);
                ResponsableMercado responsable2 = new ResponsableMercado("204778211", "Marcos", "Ramirez");

                Contrato contrato = new Contrato(1, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                                "registro1", responsable1, puesto1, cliente1);

                Contrato contrato2 = new Contrato(1, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                                "registro1", responsable2, puesto2, cliente2);

                boolean comprobacion = contrato.equals(contrato2);
                assertTrue(comprobacion);
        }

        @Test
        public void comprobarEquals_ContratosDiferentes() {
                // contrato 1
                ArrayList<Lectura> lecturas = new ArrayList<>();
                Medidor medidor1 = new Medidor(19, lecturas);
                Lectura lectura1 = new Lectura(500, LocalDate.of(2023, 1, 1), medidor1);
                medidor1.addLectura(lectura1);
                ClienteEmpresa cliente1 = new ClienteEmpresa("Cliente1", "Direccion1", 123456789, "email1@ejemplo.com",
                                "2045112781");
                Puesto puesto1 = new Puesto(1, 1000.0, true, "10x10", true, false, medidor1);
                ResponsableMercado responsable1 = new ResponsableMercado("204778211", "Marcos", "Ramirez");

                // contrato2
                ArrayList<Lectura> lecturas2 = new ArrayList<>();
                Medidor medidor2 = new Medidor(20, lecturas2);
                Lectura lectura2 = new Lectura(700, LocalDate.of(2023, 1, 1), medidor2);
                medidor2.addLectura(lectura2);
                ClienteEmpresa cliente2 = new ClienteEmpresa("Cliente1", "Direccion1", 123456789, "email1@ejemplo.com",
                                "2045112781");
                Puesto puesto2 = new Puesto(1, 1000.0, true, "10x10", true, false, medidor2);
                ResponsableMercado responsable2 = new ResponsableMercado("204778211", "Raul", "Ramirez");

                Contrato contrato = new Contrato(1, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                                "registro1", responsable1, puesto1, cliente1);

                Contrato contrato2 = new Contrato(9, LocalDate.of(2023, 1, 1), LocalDate.of(2024, 1, 1), 1000.0,
                                "registro1", responsable2, puesto2, cliente2);

                boolean comprobacion = contrato.equals(contrato2);
                assertFalse(comprobacion);
        }

}
